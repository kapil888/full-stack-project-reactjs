import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Register from "./Registation/Register";
import Dashboard from "./Dashboard/Dashboard";
import "bootstrap/dist/css/bootstrap.min.css";
import "../Css/style.css";

class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Router>
        <Route path="/" exact component={Register}></Route>
        <Route path="/Dashboard/" component={Dashboard}></Route>
      </Router>
    );
  }
}

export default Routes;
