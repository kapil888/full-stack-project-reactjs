import React, { Fragment } from "react";
import Select from "react-dropdown-select";
import Modal from "react-bootstrap/Modal";
import { Redirect } from "react-router-dom";
import Loader from "react-loader-spinner";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Personal: true,
      Acadmics: false,
      school_list: [],
      popuptype: "school",
      name: "",
      phone: "",
      graduation: [],
      post_graduation: [],
      model_open: false,
      Other: "",
      profile_obj: {},
      submit: false,
      gradution_list: [],
      Post_gradution_list: [],
      loading: false
    };
    this.final_submit = this.final_submit.bind(this);
  }

  Image_upload = e => {
    document.getElementById("files").click();
  };

  get_value = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onChange = (e, type) => {
    console.log(e);
    switch (type) {
      case "graduation":
        this.setState({
          school: e
        });
        break;
      case "post_graduation":
        this.setState({
          post_graduation: e
        });
        break;
      case "school":
        this.setState({
          school: e
        });
        break;
    }
    console.log(this.state);
  };

  show_active = e => {
    switch (e) {
      case "Personal":
        this.setState({
          Acadmics: false,
          Personal: true
        });
        break;
      case "Acadmics":
        this.setState({
          Personal: false,
          Acadmics: true
        });
        break;
    }
  };

  add_school = e => {
    this.setState({
      popuptype: e,
      model_open: true
    });
  };

  componentDidMount() {
    this.load_profile();
  }

  async load_profile() {
    try {
      var config = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Token " + localStorage.getItem("token")
        }
      };
      var response = await fetch("http://localhost:8000/profile/", config);
      var res = await response.json();
      console.log(res);
      if (res.status.isSuccess) {
        this.setState({
          profile_obj: res.status.profile,
          name: res.status.profile.name,
          Other: res.status.academica_obj.other,
          phone: res.status.profile.phone,
          loading: true,
          Post_gradution_list: res.status.academica_obj.post_graduation
            ? res.status.academica_obj.post_graduation.map(value => {
                return {
                  post_graduation_college_name: value.college_name,
                  post_graduation_course_name: value.degree_name,
                  post_graduation_course_year: value.passing_year
                };
              })
            : [],
          gradution_list: res.status.academica_obj.graduation
            ? res.status.academica_obj.graduation.map(value => {
                return {
                  graduation_collge_name: value.college_name,
                  graduation_course_name: value.degree_name,
                  graduation_course_year: value.passing_year
                };
              })
            : [],
          school_list: res.status.academica_obj.school
            ? res.status.academica_obj.school.map(value => {
                return {
                  school_Board: value.board,
                  school_Year: value.passing_year,
                  school_name: value.school_name,
                  school_class: value.student_class
                };
              })
            : []
        });
      }
    } catch (e) {
      this.setState({
        logout: true
      });
    }
  }

  add_education = () => {
    if (this.state.popuptype == "school") {
      var obj = {
        school_Board: this.state.school_Board,
        school_Year: this.state.school_Year,
        school_name: this.state.school_name,
        school_class: this.state.school_class
      };

      var data = this.state.school_list;
      data.push(obj);
      this.setState({
        school_list: data
      });
    } else if (this.state.popuptype == "graduation") {
      var obj = {
        graduation_collge_name: this.state.graduation_collge_name,
        graduation_course_name: this.state.graduation_course_name,
        graduation_course_year: this.state.graduation_course_year
      };

      var data = this.state.gradution_list;
      data.push(obj);
      this.setState({
        gradution_list: data
      });
    } else if (this.state.popuptype == "Post_Graduation") {
      var obj = {
        post_graduation_college_name: this.state.post_graduation_college_name,
        post_graduation_course_name: this.state.post_graduation_course_name,
        post_graduation_course_year: this.state.post_graduation_course_year
      };

      var data = this.state.Post_gradution_list;
      data.push(obj);
      this.setState({
        Post_gradution_list: data
      });
    }
  };

  async final_submit() {
    this.setState({ submit: true });
    var obj = {
      name: this.state.name,
      phone: this.state.phone,
      school_list: JSON.stringify(this.state.school_list),
      gradution_list: JSON.stringify(this.state.gradution_list),
      Post_gradution_list: JSON.stringify(this.state.Post_gradution_list),
      other: this.state.Other
    };
    console.log(obj);
    var config = {
      method: "POST",

      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Token " + localStorage.getItem("token")
      },
      body: JSON.stringify(obj)
    };
    var response = await fetch("http://localhost:8000/edit-profile/", config);
    var res = await response.json();
    console.log(res);
    if (res.status.isSuccess) {
      this.setState({
        submit: false
      });
    }
  }

  delete = (key, type) => {
    console.log(key);
    switch (type) {
      case "school":
        var school = [...this.state.school_list];
        school.splice(key, 1);
        this.setState({
          school_list: school
        });
        break;
      case "graduation":
        var gradution = [...this.state.gradution_list];
        gradution.splice(key, 1);
        this.setState({
          gradution_list: gradution
        });
        break;
      case "post":
        var post = [...this.state.Post_gradution_list];
        console.log(post);
        post.splice(key, 1);
        this.setState({
          Post_gradution_list: post
        });
        break;
    }
  };

  log_out = () => {
    localStorage.clear();
    this.setState({
      logout: true
    });
  };

  render() {
    if (this.state.logout) {
      return <Redirect to="/" />;
    }
    console.log(this.state);
    var data = "",
      modal_type = "";
    switch (this.state.popuptype) {
      case "school":
        modal_type = "Add school";
        data = (
          <Fragment>
            <div className="col-md-12">
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="School name"
                    className="form-control"
                    onChange={this.get_value}
                    name="school_name"
                  ></input>
                </div>
              </div>

              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Class"
                    className="form-control"
                    name="school_class"
                    onChange={this.get_value}
                  ></input>
                </div>
              </div>

              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Board"
                    className="form-control"
                    name="school_Board"
                    onChange={this.get_value}
                  ></input>
                </div>
              </div>
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Year"
                    className="form-control"
                    onChange={this.get_value}
                    name="school_Year"
                  ></input>
                </div>
              </div>
            </div>
          </Fragment>
        );

        break;

      case "graduation":
        modal_type = "Add graduation";
        data = (
          <Fragment>
            <div className="col-md-12">
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="College"
                    className="form-control"
                    name="graduation_collge_name"
                    onChange={this.get_value}
                  ></input>
                </div>
              </div>

              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Couse Name"
                    className="form-control"
                    onChange={this.get_value}
                    name="graduation_course_name"
                  ></input>
                </div>
              </div>
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Year"
                    className="form-control"
                    name="graduation_course_year"
                    onChange={this.get_value}
                  ></input>
                </div>
              </div>
            </div>
          </Fragment>
        );
        break;

      case "Post_Graduation":
        modal_type = "Add Post Graduation";
        data = (
          <Fragment>
            <div className="col-md-12">
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="College"
                    className="form-control"
                    onChange={this.get_value}
                    name="post_graduation_college_name"
                  ></input>
                </div>
              </div>

              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    placeholder="Couse Name"
                    onChange={this.get_value}
                    className="form-control"
                    name="post_graduation_course_name"
                  ></input>
                </div>
              </div>
              <div className="col-md-12 year_and_board">
                <div className="row">
                  <input
                    onChange={this.get_value}
                    placeholder="Year"
                    className="form-control"
                    name="post_graduation_course_year"
                  ></input>
                </div>
              </div>
            </div>
          </Fragment>
        );
        break;
    }

    if (this.state.loading) {
      return (
        <div className="container">
          <div className="dashbord_profile_container">
            <div className="dashboard_image_pannel col-md-4">
              <div className="col-md-12 profile_conatiner_image" align="center">
                <img
                  src={require("../../Images/banner.jpg")}
                  className="profile_image_style"
                ></img>
                <h1 className="name_Style">{this.state.profile_obj.name}</h1>
              </div>

              <div className="profile_navigation">
                <ul>
                  <li
                    className={this.state.Personal ? "active" : ""}
                    onClick={this.show_active.bind(this, "Personal")}
                  >
                    Personal
                  </li>
                  <li
                    className={this.state.Acadmics ? "active" : ""}
                    onClick={this.show_active.bind(this, "Acadmics")}
                  >
                    Acadmics
                  </li>
                </ul>
              </div>
            </div>

            {this.state.Personal ? (
              <div className="dashboard_info_pannel col-md-8">
                <button
                  onClick={this.log_out}
                  className="btn btn-primary profile_save_btn"
                >
                  Log out
                </button>
                <h3>Personal</h3>
                <label>Name</label>
                <input
                  className="form-control"
                  name="name"
                  value={this.state.name}
                  onChange={this.get_value}
                  placeholder="Name"
                ></input>

                <label>Phone</label>
                <input
                  className="form-control"
                  value={this.state.phone}
                  name="phone"
                  onChange={this.get_value}
                  placeholder="phone"
                ></input>

                <label>Update image</label>
                <p
                  class="form-control file_upload_conatiner"
                  onClick={this.Image_upload}
                >
                  <span className="upload_image">Upload image</span>
                </p>
                <input
                  type="file"
                  id="files"
                  style={{ display: "none" }}
                  onChange={this.get_image}
                />

                {this.state.submit ? (
                  <Loader
                    type="Puff"
                    className="small_loader_dashboard"
                    color="#00BFFF"
                    height={100}
                    width={40}
                  />
                ) : (
                  <button
                    onClick={this.final_submit}
                    className="btn btn-primary profile_save_btn"
                  >
                    Save
                  </button>
                )}
              </div>
            ) : (
              <div className="dashboard_info_pannel col-md-8">
                <button
                  onClick={this.log_out}
                  className="btn btn-primary profile_save_btn"
                >
                  Log out
                </button>
                <div className="seprate">
                  <h3>Acadmics</h3>
                  <label>School</label>
                  <p
                    className="form-control btn btn-primary"
                    onClick={this.add_school.bind(this, "school")}
                  >
                    Add school
                  </p>

                  <table>
                    <tbody>
                      {this.state.school_list.map((value, key) => {
                        return (
                          <tr key={key}>
                            <td>{value.school_name}</td>
                            <td>{value.school_Year}</td>
                            <td>{value.school_Board}</td>
                            <td>
                              <img
                                onClick={this.delete.bind(this, key, "school")}
                                style={{ height: 20, width: 20 }}
                                src="https://p7.hiclipart.com/preview/331/214/518/scalable-vector-graphics-computer-file-cancel-button-png-photos.jpg"
                              ></img>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>

                <Modal
                  show={this.state.model_open}
                  onHide={() => this.setState({ model_open: false })}
                >
                  <Modal.Header>
                    <Modal.Title>{modal_type}</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>{data}</Modal.Body>
                  <Modal.Footer>
                    <button
                      onClick={this.add_education}
                      className="btn btn-primary profile_save_btn"
                    >
                      Add
                    </button>
                  </Modal.Footer>
                </Modal>

                <div className="seprate">
                  <label>Graduation</label>
                  <p
                    className="form-control btn btn-primary"
                    onClick={this.add_school.bind(this, "graduation")}
                  >
                    Add graduation
                  </p>

                  <table>
                    <tbody>
                      {this.state.gradution_list.map((value, key) => {
                        return (
                          <tr key={key}>
                            <td>{value.graduation_collge_name}</td>
                            <td>{value.graduation_course_year}</td>
                            <td>{value.graduation_course_name}</td>
                            <td>
                              <img
                                onClick={this.delete.bind(
                                  this,
                                  key,
                                  "graduation"
                                )}
                                style={{ height: 20, width: 20 }}
                                src="https://p7.hiclipart.com/preview/331/214/518/scalable-vector-graphics-computer-file-cancel-button-png-photos.jpg"
                              ></img>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>

                <div className="seprate">
                  <label>Post Graduation</label>
                  <p
                    className="form-control btn btn-primary"
                    onClick={this.add_school.bind(this, "Post_Graduation")}
                  >
                    Post Graduation
                  </p>

                  <table>
                    <tbody>
                      {this.state.Post_gradution_list.map((value, key) => {
                        return (
                          <tr key={key}>
                            <td>{value.post_graduation_college_name}</td>
                            <td>{value.post_graduation_course_year}</td>
                            <td>{value.post_graduation_course_name}</td>
                            <td>
                              <img
                                onClick={this.delete.bind(this, key, "post")}
                                style={{ height: 20, width: 20 }}
                                src="https://p7.hiclipart.com/preview/331/214/518/scalable-vector-graphics-computer-file-cancel-button-png-photos.jpg"
                              ></img>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>

                <div className="seprate">
                  <label>Other</label>
                  <input
                    type="text"
                    className="form-control "
                    placeholder="Other"
                    value={this.state.Other}
                    onChange={this.get_value}
                    name="Other"
                  />
                </div>

                {this.state.submit ? (
                  <Loader
                    type="Puff"
                    className="small_loader_dashboard"
                    color="#00BFFF"
                    height={100}
                    width={40}
                  />
                ) : (
                  <button
                    onClick={this.final_submit}
                    className="btn btn-primary profile_save_btn"
                  >
                    Save
                  </button>
                )}
              </div>
            )}
          </div>
        </div>
      );
    } else {
      return (
        <Loader
          type="Puff"
          className="loader"
          color="#00BFFF"
          height={100}
          width={100}
        />
      );
    }
  }
}

export default Dashboard;
