import React from "react";
import { Link, Redirect } from "react-router-dom";

import Loader from "react-loader-spinner";

var validator = require("email-validator");

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Name: "",
      Email: "",
      step: 2,
      Phone: "",
      password: "",
      animation: true,
      post_graduation: [],
      school: [],
      graduation: [],
      error_message: "",
      image: "",
      login_Clicked: false,
      error_login_message: "",
      login_email: "",
      login_password: "",
      data: [{ value: "text", label: "text" }, { value: "p", label: "p" }]
    };
    this.registation = this.registation.bind(this);
    this.login = this.login.bind(this);
  }

  componentDidMount() {}

  async registation() {
    var config = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.state.Name,
        email: this.state.Email,
        phone: this.state.Phone,
        password: this.state.password
      })
    };
    var response = await fetch("http://localhost:8000/registation/", config);
    var res = await response.json();
    console.log(res);
    if (res.status.isSuccess) {
      localStorage.setItem("token", res.status.token);
      this.setState({
        go_to_dashboard: true
      });
    }
  }

  async login() {
    console.log(validator.validate(this.state.login_email));
    this.setState({ login_Clicked: true });
    if (
      validator.validate(this.state.login_email) &&
      this.state.login_password.length > 0
    ) {
      var data = {
        email: this.state.login_email,
        password: this.state.login_password
      };

      var config = {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      };
      var response = await fetch("http://localhost:8000/login/", config);
      var res = await response.json();

      if (res.status.isSuccess) {
        localStorage.setItem("token", res.status.token);
        this.setState({
          go_to_dashboard: true,
          login_Clicked: false,
          error_login_message: ""
        });
      } else {
        this.setState({
          go_to_dashboard: false,
          login_Clicked: false,
          error_login_message:
            res.status.error == "User matching query does not exist"
              ? "Email not exist"
              : res.status.error
        });
      }
    } else {
      if (!validator.validate(this.state.Email)) {
        this.setState({
          error_login_message: "Please Enter Email",
          login_Clicked: false
        });
      } else if (this.state.login_password.length == 0) {
        this.setState({
          error_login_message: "Please Enter password",
          login_Clicked: false
        });
      }
    }
  }

  Image_upload = e => {
    document.getElementById("files").click();
  };

  get_value = e => {
    if (e.target.name == "Phone") {
      if (e.target.value.toString().length <= 10) {
        this.setState({
          [e.target.name]: e.target.value.replace(/[^0-9]/g, "")
        });
      }
    } else {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
  };

  onChange = (e, type) => {
    console.log(e);
    switch (type) {
      case "graduation":
        this.setState({
          school: e
        });
        break;
      case "post_graduation":
        this.setState({
          post_graduation: e
        });
        break;
      case "school":
        this.setState({
          school: e
        });
        break;
    }
    console.log(this.state);
  };

  next_button = () => {
    if (
      this.state.Name.length > 0 &&
      validator.validate(this.state.Email) &&
      this.state.Phone.length == 10 &&
      this.state.password.length > 0
    ) {
      this.registation();
    } else {
      if (this.state.Name.length == 0) {
        this.setState({
          error_message: "Please enter a name"
        });
      } else if (!validator.validate(this.state.Email)) {
        this.setState({
          error_message: "Please enter a Email"
        });
      } else if (this.state.Phone.length == 0) {
        this.setState({
          error_message: "Please enter a Phone"
        });
      } else if (this.state.password.length == 0) {
        this.setState({
          error_message: "Please enter a password"
        });
      }
    }
  };

  changeScreens = () => {
    this.setState({ step: this.state.step == 1 ? 2 : 1 });
  };

  render() {
    if (this.state.go_to_dashboard) {
      return <Redirect to="/dashboard/"></Redirect>;
    }

    if (localStorage.getItem("token")) {
      return <Redirect to="/dashboard/"></Redirect>;
    }
    return (
      <div className="container">
        <div className="main_cnatiner">
          <div className="col-md-6 left_info_container">
            <div className="left_info_child">
              <h1 className="welcome_heading">Learning on the go !!</h1>

              <p className="welcome_description">
                it is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
                normal distribution of letters, as opposed to using 'Content
                here, content here.
              </p>
            </div>
          </div>

          <div className="col-md-6 registation_container">
            {/* <div className="col-md-12" align="center">
              <span
                className="step1"
                style={
                  this.state.step == 1
                    ? { backgroundColor: "#1e8cc1" }
                    : { backgroundColor: "#ccc" }
                }
              >
                1
              </span>
              <span className="step-seprater"></span>
              <span
                className="step2"
                style={
                  this.state.step == 2
                    ? { backgroundColor: "#1e8cc1" }
                    : { backgroundColor: "#ccc" }
                }
              >
                2
              </span>
            </div> */}

            {this.state.step == 1 ? (
              <div className="step_one_conatiner">
                <h1 className="header">Register</h1>
                <input
                  type="text"
                  class="form-control input_style"
                  placeholder="Name"
                  value={this.state.name}
                  onChange={this.get_value}
                  name="Name"
                />
                <input
                  type="text"
                  class="form-control input_style"
                  placeholder="Email"
                  value={this.state.Email}
                  name="Email"
                  onChange={this.get_value}
                />
                <input
                  type="text"
                  class="form-control input_style"
                  placeholder="Phone"
                  value={this.state.Phone}
                  onChange={this.get_value}
                  name="Phone"
                />

                <input
                  type="password"
                  class="form-control input_style"
                  placeholder="password"
                  onChange={this.get_value}
                  value={this.state.password}
                  name="password"
                />

                <p className="error_message">{this.state.error_message}</p>
                <input
                  type="file"
                  id="files"
                  style={{ display: "none" }}
                  onChange={this.get_image}
                />

                <span
                  onClick={this.changeScreens}
                  className="login_signup_button"
                >
                  Login
                </span>
                <button
                  onClick={this.next_button}
                  style={{ float: "right", marginTop: 20 }}
                  className="btn btn-primary flot-right"
                >
                  Register
                </button>
              </div>
            ) : (
              <div className="step_two_container">
                <h1 className="header">Login</h1>
                <input
                  type="text"
                  class="form-control input_style"
                  placeholder="Email"
                  value={this.state.login_email}
                  onChange={this.get_value}
                  name="login_email"
                />

                <input
                  type="password"
                  class="form-control input_style"
                  placeholder="password"
                  value={this.state.login_password}
                  onChange={this.get_value}
                  name="login_password"
                />

                <p className="error_message">
                  {this.state.error_login_message}
                </p>

                <span
                  onClick={this.changeScreens}
                  className="login_signup_button"
                >
                  Signup
                </span>

                <div className="col-md-12">
                  {this.state.login_Clicked ? (
                    <Loader
                      type="Puff"
                      className="small_loader"
                      color="#00BFFF"
                      height={100}
                      width={40}
                    />
                  ) : (
                    <button
                      onClick={this.login}
                      style={{ float: "right", marginTop: 20 }}
                      class="btn btn-primary flot-right"
                    >
                      Login
                    </button>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
